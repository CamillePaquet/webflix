import React from 'react';

import Chip from '../components/Chip.js';

export default {
  title: 'Chip',
  component: Chip
};

const Template = () => <Chip id={'10752'} />;

export const genre = Template.bind({});
