import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(() => ({
  root: {
    background: 'black',
    color: 'white',
    padding: '6px 10px',
    display: 'inline-block'
  }
}));

export default useStyles;
