import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(() => ({
  similarList: {
    display: 'flex',
    overflow: 'scroll'
  }
}));

export default useStyles;
