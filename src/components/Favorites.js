import GridCards from './GridCards.js';
import { useSelector } from 'react-redux';
function Favorites(props) {
  const films = useSelector((state) => state.favorites);

  // console.log(films)

  return (
    <div>
      <GridCards
        data={films}
        addToFavorite={props.addToFavorite}
        favorites={props.favorites}></GridCards>
    </div>
  );
}

export default Favorites;
