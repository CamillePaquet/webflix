import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(() => ({
  inputSearch: {
    borderStyle: 'solid',
    borderWidth: '2px',
    borderColor: 'black',
    height: 25
  }
}));

export default useStyles;
