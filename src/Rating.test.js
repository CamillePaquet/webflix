import Rating from './components/Rating';
import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

describe(Rating, () => {
  it('renders a div', () => {
    render(<Rating note={10}></Rating>);
    expect(screen.getAllByRole('img').first).toBeInTheDocument;
  });
});
